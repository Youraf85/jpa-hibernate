package com.youraf.jpa.hibernate.jpahibernate;

import java.math.BigDecimal;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.youraf.jpa.hibernate.jpahibernate.entity.Course;
import com.youraf.jpa.hibernate.jpahibernate.entity.FullTimeEmployee;
import com.youraf.jpa.hibernate.jpahibernate.entity.PartTimeEmployee;
import com.youraf.jpa.hibernate.jpahibernate.repository.CourseRepository;
import com.youraf.jpa.hibernate.jpahibernate.repository.EmployeeRepository;

@SpringBootApplication
public class JpaHibernateApplication implements CommandLineRunner{
	
	private Logger logger=LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	private CourseRepository courseDao; 
	
	@Autowired
	private EmployeeRepository empDao; 

	public static void main(String[] args) {
		SpringApplication.run(JpaHibernateApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
//		Course course= courseDao.findById(10001L);
//		logger.info("Course 10001= > {}", course);
//		
//		courseDao.save(new Course("Microservices")); 
		
		//courseDao.playWithEM();
		
		empDao.insert(new PartTimeEmployee("youssef", new BigDecimal("25")));
		empDao.insert(new FullTimeEmployee("mouadd", new BigDecimal("2000")));
		
		logger.info("All Full Employes {}",empDao.getAllFullTimeEmployees());
		logger.info("All Part Employes {}",empDao.getAllPartTimeEmployees());
	}

}
