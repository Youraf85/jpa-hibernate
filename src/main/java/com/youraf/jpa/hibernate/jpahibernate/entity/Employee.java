package com.youraf.jpa.hibernate.jpahibernate.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;


// if u want to create one table for all type
// @Inheritance(strategy = InheritanceType.SINGLE_TABLE)
// add a column that will have the subclass names
// @DiscriminatorColumn(name="EmployeeType")
//if u want to create one table for each entity
// @Entity
// @Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
// @Inheritance(strategy = InheritanceType.JOINED)
@MappedSuperclass
public abstract class Employee {

	@Id
	@GeneratedValue
	private Long id;
	@Column(nullable = false)	
	private String name;
	
	
	public Employee() {
	}
	public Employee(String name) {
		this.name = name;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	
	@Override
	public String toString() {
		return String.format("Employee [%s]",name);
	}
	
}
