package com.youraf.jpa.hibernate.jpahibernate.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.youraf.jpa.hibernate.jpahibernate.entity.Course;
import com.youraf.jpa.hibernate.jpahibernate.entity.Review;

@Repository
@Transactional
public class CourseRepository {
	
	Logger logger=LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	EntityManager em;
	
	public Course findById(Long id) {
		return em.find(Course.class, id);
	}
	
	public void jpqlBasic() {
		Query query=em.createQuery("Select c from Course c");
		List resultList=query.getResultList();
		logger.info("Select c from Course  c->{}", resultList);
	}
	
	public void jpqlTyped() {
		TypedQuery<Course> query=em.createQuery("Select c from Course c",Course.class);
		List<Course> resultList=query.getResultList();
		logger.info("Select c from Course  c->{}", resultList);
	}
	
	public Course save(Course course) {
		if(course.getId()==null) {
			em.persist(course);
		}else {
			em.merge(course);
		}
		return course;
	}
	
	public void deleteById(Long id) {
		Course c=findById(id);
		em.remove(c);
	}

	public void playWithEM() {
		Course course1=new Course("LEarn Jpa Hibernate");
		em.persist(course1);
		Course course2=new Course("LEarn Angular");
		em.persist(course2);
		//send changes to db
		em.flush();
		
		// detach course1 so the changes will not be sent to db
		em.detach(course1);
		// em.clear(); detach all object
		course1.setName("LEarn Jpa Hibernate in Three days");
		course2.setName("LEarn Angular in five days");
		
		em.refresh(course2);// ignore changes by getting the value from DB
		// only cours2 will be changed in DB
		em.flush();
	}

	public void namedQuery() {
		TypedQuery<Course> query=em.createNamedQuery("query_get_all_courses",Course.class);
		List<Course> list=query.getResultList();
		
		logger.info("course using named query: {}",list);
	}
	
	void addReviewsForCourse(Course course,List<Review> reviews) {
		for(Review rev:reviews) {
			course.addReview(rev);
			rev.setCourse(course);
			em.persist(rev);
		}
	}
}
