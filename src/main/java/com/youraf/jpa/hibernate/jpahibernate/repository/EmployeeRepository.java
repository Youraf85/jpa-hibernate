package com.youraf.jpa.hibernate.jpahibernate.repository;

import java.util.List;

import javax.persistence.EntityManager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.youraf.jpa.hibernate.jpahibernate.entity.Employee;

@Repository
@Transactional
public class EmployeeRepository {
	
	Logger logger=LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	EntityManager em;
	
	public Employee findById(Long id) {
		return em.find(Employee.class, id);
	}
	
	public void insert(Employee emp) {
		em.persist(emp);
	}
	
	public List<Employee> getAllFullTimeEmployees(){
		return em.createQuery("select e from FullTimeEmployee e",Employee.class).getResultList();
	}
	
	public List<Employee> getAllPartTimeEmployees(){
		return em.createQuery("select e from PartTimeEmployee e",Employee.class).getResultList();
	}
}
