package com.youraf.jpa.hibernate.jpahibernate.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
 
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.youraf.jpa.hibernate.jpahibernate.entity.Student;

@Repository
@Transactional
public class StudentRepository {
	
	Logger logger=LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	EntityManager em;
	
	public Student findById(Long id) {
		return em.find(Student.class, id);
	}
	
	public void jpqlBasic() {
		Query query=em.createQuery("Select c from Student c");
		List resultList=query.getResultList();
		logger.info("Select c from Student  c->{}", resultList);
	}
	
	public void jpqlTyped() {
		TypedQuery<Student> query=em.createQuery("Select c from Student c",Student.class);
		List<Student> resultList=query.getResultList();
		logger.info("Select c from Student  c->{}", resultList);
	}
	
	public Student save(Student student) {
		if(student.getId()==null) {
			em.persist(student);
		}else {
			em.merge(student);
		}
		return student;
	}
	
	public void deleteById(Long id) {
		Student c=findById(id);
		em.remove(c);
	}


	public void namedQuery() {
		TypedQuery<Student> query=em.createNamedQuery("query_get_all_students",Student.class);
		List<Student> list=query.getResultList();
		
		logger.info("student using named query: {}",list);
	}
}
