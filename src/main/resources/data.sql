insert into course(id, name) values(10001,'JPA Course');
insert into course(id, name) values(10002,'Spring Course');
insert into course(id, name) values(10003,'Angular Course');

insert into passport(id, number) values(3001,'BJ45566');
insert into passport(id, number) values(3002,'EA34566');

insert into student(id, name, passport_id) values(2001,'Youssef',3001);
insert into student(id, name, passport_id) values(2002,'Taha',3002);



insert into review(id, rating, description,course_id) values(4001,'5','Great course',10001);
insert into review(id, rating, description,course_id) values(4002,'4','good understading',10002);