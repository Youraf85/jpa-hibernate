package com.youraf.jpa.hibernate.jpahibernate.repository;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.youraf.jpa.hibernate.jpahibernate.JpaHibernateApplication;
import com.youraf.jpa.hibernate.jpahibernate.entity.Course;
import com.youraf.jpa.hibernate.jpahibernate.entity.Review;

import static org.junit.jupiter.api.Assertions.*;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.transaction.Transactional;

@ExtendWith(SpringExtension.class)
@SpringBootTest(classes=JpaHibernateApplication.class)
class CourseRepositoryTest {
	
	private Logger logger=LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	CourseRepository daoRepo;
	
	@Autowired
	EntityManager em;
	

	@Test
	void findByID_test() {
		Course c=daoRepo.findById(10001L);
		assertEquals("JPA Course",c.getName());
	}
	
	@Test
	@DirtiesContext // permet de remettre les données modifiés
	void deleteById_test() {
		daoRepo.deleteById(10002L);
		assertNull(daoRepo.findById(10002L));
	}
	
	@Test
	@DirtiesContext
	void save_test() {
		Course c=daoRepo.findById(10001L);
		assertEquals("JPA Course",c.getName());
		
		c.setName("JPA Course V2");
		daoRepo.save(c);
		
		Course c2=daoRepo.findById(10001L);
		assertEquals("JPA Course V2",c2.getName());
	}
	
	@Test
	@DirtiesContext
	void playWithEM_test() {
		daoRepo.playWithEM();;
	}
	
	
	@Test
	@DirtiesContext
	void namedQuery() {
		daoRepo.namedQuery();;
	}
	
	@Test
	@Transactional
	public void addReviewsForCourse() {
		Course course=daoRepo.findById(10001L);
		
		Review rev=new Review("3","NOt so good");
		
		course.addReview(rev);
		rev.setCourse(course);
		
		em.persist(rev);
		
	}

}
