package com.youraf.jpa.hibernate.jpahibernate.repository;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import javax.persistence.EntityManager;

import org.hibernate.LazyInitializationException;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.youraf.jpa.hibernate.jpahibernate.JpaHibernateApplication;
import com.youraf.jpa.hibernate.jpahibernate.entity.Student;

@ExtendWith(SpringExtension.class)
@SpringBootTest(classes=JpaHibernateApplication.class)
class StudentRepositoryTest {
	
	private Logger logger=LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	StudentRepository daoRepo;
	
	@Autowired
	EntityManager em;
	

	@Test
	void retreiveStudantAndPassport() {

		//logger.info("passport {}",student.getPassport());
		Student student=em.find(Student.class, 2001L);
		logger.info("student {}",student);
		student.getPassport().getId();
		Exception exception = assertThrows(LazyInitializationException.class,
				() -> {

				}
		);
	 
	    String expectedMessage = "could not initialize proxy";
	    String actualMessage = exception.getMessage();
	 
	    assertTrue(actualMessage.contains(expectedMessage));
	}
	
	
	

}
